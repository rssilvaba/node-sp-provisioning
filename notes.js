//* the way that content types works, is that we need first a content type, 
//* we need site columns, that later will be our columns in our content types
//*   we add the site columns we want to the content type.
//*   we create the list then go the adv settings and enable content types
//*   we move the content type we want to the top and delete the old one
//*   now the list will have the content type wanted and it's fields.

//* each config file should not have inter dependencies (meaning should not modules by itself), 
//*   this ensures that they will work on browser and on node.
//*   we can later on inject these dependencies from the build script for each env.

//* when adding a webpart that is a list we should right after provisioning that webpart, change the list view 
//*   to visible, and set an unique identifier (title), preferable not the id since it is created in place and we 
//*   would need to store somewhere. this is important because the title of a list view wp is null and it is 
//*   hidden from the list settings.

// TODO add update webpart def