// @ts-check
/**
 * @template {string[]} A
 * @template {{[key: string]: any}} B
 * @param {A} propsToKeep
 * @param {B} obj
 * @returns {Pick<B, Exclude<keyof B, Exclude<keyof B, A>>>}
 */
export const keepProps = (propsToKeep, obj) => Object.assign({}, ...propsToKeep
  .filter((prop) => Object.hasOwnProperty.call(obj, prop))
  .map((prop) => ({ [prop]: obj[prop] })));

// const keep = (keyList, obj) => {
// eslint-disable-next-line max-len
//   const reducer = (acc, cv) => obj[cv] !== null && obj[cv] !== '' && obj[cv] !== undefined ? { ...acc, [cv]: obj[cv] } : acc
//   return keyList.reduce(reducer, {})
// }
