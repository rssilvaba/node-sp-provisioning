// @ts-check

import { keepProps } from './keep-props.mjs';

export const TestKeepProps = () => {
  const result = keepProps(['one', 'two'], { one: 1, two: 2, three: 3 });
  return JSON.stringify(result) === JSON.stringify({ one: 1, two: 2 });
};
