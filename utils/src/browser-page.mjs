// @ts-check

import fetch from 'node-fetch';
import puppeteer from 'puppeteer-core';

let debugJson = { webSocketDebuggerUrl: '' };
// eslint-disable-next-line init-declarations
/** @type {puppeteer.Browser} */ let browser;
// eslint-disable-next-line init-declarations
/** @type {puppeteer.Page | undefined} */ let page;

/**
 * @template A,E
 * @param {Promise<A>} prom
 * @param {number} time
 * @returns {Promise.<A|E>}
 */
const timeout = (prom, time) => Promise.race(
  // eslint-disable-next-line no-promise-executor-return
  [prom, new Promise((_r, rej) => setTimeout(rej, time))],
);

/**
 * @param {string} browserRemoteDebuggerUrl
 * @param {string} pageUrlToMatch
 * @param {string} selector
 * @returns {Promise.<puppeteer.Page| Error>}
 */
export const browserPage = async (browserRemoteDebuggerUrl, pageUrlToMatch, selector) => {
  try {
    if (!browser && !page) {
      debugJson = await (await timeout(fetch(browserRemoteDebuggerUrl), 1000)).json();
      browser = await puppeteer.connect({
        browserWSEndpoint: debugJson.webSocketDebuggerUrl,
        defaultViewport: null,
      });
      const pages = await browser.pages();
      page = pages.find((x) => x.url() === pageUrlToMatch);
      if (selector) {
        await page?.waitForSelector(selector);
      }
    }
    return page || new Error('page not found');
  } catch (/** @type {any} */error) {
    return error;
  }
};
