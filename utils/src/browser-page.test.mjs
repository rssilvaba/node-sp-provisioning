// @ts-check
import { browserPage } from './browser-page.mjs';
import { tryCatch } from './fp-utils.mjs';

export const TestBrowserPage = async () => {
  const browserRemoteDebuggerUrl = 'http://127.0.0.1:9222/json/version';
  const pageUrlToMatch = 'https://rssilvaba.sharepoint.com/sites/BBNW/_layouts/15/viewlsts.aspx';
  /** @type {import("puppeteer-core").Page} */
  const page = await tryCatch(() => browserPage(browserRemoteDebuggerUrl, pageUrlToMatch, null));
  page?.browser?.().disconnect();
  return page instanceof Error ? page : page?.url?.() === pageUrlToMatch;
};
TestBrowserPage.description = 'TestBrowserPage E success path';
