export const tryCatch = (f) => {
  try { return f(); } catch (e) { return e; }
};

export function forEach(array, f) {
  for (let i = 0; i < array.length; i += 1) {
    const item = array[i];
    f(item);
  }
}

export function mapAsync(array, f) {
  const newArray = [];
  forEach(array, (element) => {
    newArray.push(f(element));
  });
  return Promise.all(newArray);
}

export function map(array, f) {
  const newArray = [];
  forEach(array, (element) => {
    newArray.push(f(element));
  });
  return newArray;
}
