// @ts-check
import fs from 'fs';
import util from 'util';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

const readFile = util.promisify(fs.readFile);

/**
 * @param {string} path_
 */
export async function importer(path_) {
  // console.log(__dirname);
  // const data = await readFile(path_, { encoding: 'utf-8' });
  // eslint-disable-next-line max-len
  // const data = await readFile(path.join(process.cwd(), path_).replace(/\\/g, '/'), { encoding: 'utf-8' });
  // eslint-disable-next-line max-len
  // const data = await readFile(`./${path.relative(process.cwd(), path.join(__dirname, path_))}`.replace(/\\/g, '/'), { encoding: 'utf-8' });
  const data = await readFile(path.join(__dirname, '../../', path_), { encoding: 'utf-8' });
  return `data:text/javascript;utf-8,${encodeURIComponent(data)}`;
}
