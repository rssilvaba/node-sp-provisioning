import { importer } from './importer.mjs';

export const TestImporter01 = async () => {
  // TODO how to avoid absolute paths?
  // eslint-disable-next-line max-len
  //* the issue here is that if I use relative paths it tries to resolve the path from where the node command was executed.
  const file = await importer('utils/src/importer.file-mock.mjs');
  return decodeURIComponent(file).includes('data:text/javascript;utf-8') && decodeURIComponent(file).includes('export const two = () => 2');
};
