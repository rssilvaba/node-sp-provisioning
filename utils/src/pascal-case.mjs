/**
 * @param {string} str string @returns {string} string */
export function toPascalCase(str) {
  return `${str}`
    .replace(/[-_]+/g, ' ')
    .replace(/[^\w\s]/g, '')
    .replace(
      /\s+(.)(\w*)/g,
      ($1, $2, $3) => `${$2.toUpperCase() + $3.toLowerCase()}`,
    )
    .replace(/\w/, (s) => s.toUpperCase())
    .trim();
}
// console.log(toPascalCase("placerat❤️consectetuer").length === 'placeratconsectetuer'.length);
// eslint-disable-next-line max-len
// export const toPascalCase = (str) => (str.match(/[a-zA-Z0-9]+/g) || []).map((w) => `${w.charAt(0).toUpperCase()}${w.slice(1)}`).join('');
