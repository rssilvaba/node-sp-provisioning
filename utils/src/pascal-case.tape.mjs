// // import test from 'tape';
// // import fc from 'fast-check';
// // import { toPascalCase } from './pascal-case.mjs';
// // should always only have a-Z and 0-9 chars === should never have special chars
// // should always start with a Number or A uppercase letter === should never start special chars
// // if the input is already in PascalCase the output should not change

// // Code under test

// test('if the input is already in PascalCase the output should not change', (assert) => {
//   assert.plan(1);
//   assert.doesNotThrow(() => {
//     fc.assert(
//       fc.property(fc.constant('PascalCase'), (a) => toPascalCase(a) === a),
//     );
//   });
// });

// test('for any lorem word the length is always the same', (assert) => {
//   // assert.plan(1);
//   assert.doesNotThrow(() => {
//     fc.assert(
// eslint-disable-next-line max-len
//       fc.property(fc.lorem({ maxCount: 1, mode: 'words' }), (a) => toPascalCase(a).length === a.length),
//     );
//   });
// });

// /* bug */ const contains = (pattern, text) => text.substr(1).indexOf(pattern) !== -1;
// // const contains = (pattern, text) => text.indexOf(pattern) !== -1;

// test('The concatenation of a, b and c always contains b', (assert) => {
//   assert.plan(1);
//   assert.doesNotThrow(() => {
//     fc.assert(
//       fc.property(fc.string(), fc.string(), fc.string(), (a, b, c) => contains(b, a + b + c)),
//     );
//   });
// });
