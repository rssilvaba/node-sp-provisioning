// @ts-check
import fc from 'fast-check';
import { tryCatch } from './fp-utils.mjs';

const { toPascalCase } = await import('./pascal-case.mjs');

export const TestToPascalCase01 = () => toPascalCase('BBMW') === 'BBMW';
export const TestToPascalCase02 = () => toPascalCase('testPascalCase') === 'TestPascalCase';
export const TestToPascalCase03 = () => toPascalCase('testPascal Case') === 'TestPascalCase';
export const TestToPascalCase04 = () => toPascalCase('testPascal Case%$') === 'TestPascalCase';
export const TestToPascalCase05 = () => toPascalCase('testPascal Case%$(*()^*') === 'TestPascalCase';
export const TestToPascalCase06 = () => toPascalCase('test pascal Case%$(*()^*') === 'TestPascalCase';
export const TestToPascalCase07 = () => toPascalCase('_test pascal Case%$(*()^*') === 'TestPascalCase';
export const TestToPascalCase08 = () => toPascalCase('foo bar') === 'FooBar';
export const TestToPascalCase09 = () => toPascalCase('Foo Bar') === 'FooBar';
export const TestToPascalCase10 = () => toPascalCase('fooBar') === 'FooBar';
export const TestToPascalCase11 = () => toPascalCase('FooBar') === 'FooBar';
export const TestToPascalCase12 = () => toPascalCase('--foo-bar--') === 'FooBar';
export const TestToPascalCase13 = () => toPascalCase('__FOO_BAR__') === 'FooBar';
export const TestToPascalCase14 = () => toPascalCase('!--foo-¿?-bar--121-**%') === 'FooBar121';
export const TestToPascalCase15 = () => toPascalCase('Here i am') === 'HereIAm';
export const TestToPascalCase16 = () => toPascalCase('2Here i am') === '2HereIAm';

export const TestToPascalCase17 = () => tryCatch(
  () => fc.assert(fc.property(
    fc.constant('PascalCase'),
    // @ts-ignore
    (a) => toPascalCase(a) === a,
  ), { verbose: true }) === undefined,
);
TestToPascalCase17.description = 'TestToPascalCase17 P input and output is the same if already in PascalCase';

export const TestToPascalCase18 = () => tryCatch(() => fc.assert(fc.property(
  fc.lorem({ maxCount: 1, mode: 'words' }),
  // @ts-ignore
  (a) => toPascalCase(a).length === a.length,
), { verbose: true }) === undefined);
TestToPascalCase18.description = 'TestToPascalCase18 P for any single word the length is always the same';

export const TestToPascalCase19 = () => tryCatch(() => fc.assert(fc.property(
  fc.lorem({ maxCount: 1, mode: 'words', size: 'max' }),
  // @ts-ignore
  fc.stringOf(fc.constantFrom('❤️', '😊', '_', '-', '%', '     ', '^%', ' ')),
  fc.lorem({ maxCount: 1, mode: 'words', size: 'max' }),
  // @ts-ignore
  (a, b, c) => toPascalCase(a.concat(b).concat(c)).length === (a.concat(c)).length,
), { verbose: true }) === undefined);
TestToPascalCase19.description = 'TestToPascalCase19 P for any two single word with any number of "special" chars in between, length is the same minus "special" chars';

// /**
//  * @typedef {Object} Test
//  * @property {string} name
//  * @property {string} type
//  * @property {string[]} args
//  * @property {boolean} expected
//  */
//  const description = (/** @type {Test} */test) => `${test.name} ${test.type} ${JSON.stringify(test.args)} -> ${test.expected}`;

//  export const exampleTests = {
//    TestToPascalCase01: {
//      args: ['BBMW'],
//      expected: 'BBMW',
//      fn: (str, exp) => toPascalCase(str) === exp,
//    },
//    TestToPascalCase02: {
//      args: ['testPascalCase'],
//      expected: 'testPascalCase',
//    },
//  };

//  export const propertyTests = {
//    TestToPascalCase01: {
//      args: ['BBMW'],
//      expected: 'BBMW',
//    },
//    TestToPascalCase02: {
//      args: ['testPascalCase'],
//      expected: 'testPascalCase',
//    },
//  };

//  // const examplesTable = [
//  //   {
//  //     type: 'E',
//  //     name: '',
//  //     args: ['BBMW'],
//  //     expected: 'BBMW',
//  //   },
//  //   {
//  //     type: 'E',
//  //     name: 'TestToPascalCase01',
//  //     args: ['testPascalCase'],
//  //     expected: 'TestPascalCase',
//  //   },
//  // ]
