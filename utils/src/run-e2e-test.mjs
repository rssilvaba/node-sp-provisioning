// @ts-check
import glob from 'glob'
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { browserRemoteDebuggerUrl, pageSelector, pageUrlToMatch } from "../env.mjs";
import { browserPage } from './browser-page.mjs';
import { importer } from "./importer.mjs";

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

export async function runE2eTest(globString) {
  let passed = 0;
  let failed = 0;
  let result;
  const files = glob.sync(globString);
  const page = await browserPage(browserRemoteDebuggerUrl, pageUrlToMatch, pageSelector);
  for (let i = 0; i < files.length; i++) {
    // eslint-disable-next-line no-await-in-loop
    const module = await import(`./${path.relative(__dirname, files[i])}`);
    for (let z = 0; z < Object.keys(module).length; z++) {
      const testFn = Object.keys(module)[z];
      // eslint-disable-next-line no-await-in-loop
      const deps = await import(`./${path.relative(__dirname, files[i])}`);
      try {
        // eslint-disable-next-line no-await-in-loop
        const dep = await importer(deps[testFn].module);
        // eslint-disable-next-line no-await-in-loop
        result = await page.evaluate(deps[testFn], dep);
        // eslint-disable-next-line no-plusplus
        if (result) { passed++ } else { failed++ };
        console.log(`${result ? `✅` : `❌`} ${module[testFn].description || testFn}`)
      } catch (e) {
        failed++
        result = false;
        console.log(`${result ? `✅` : `❌`} ${module[testFn].description || testFn}`)
        console.error(e)
      }
    }
  }
  console.log(`\n✅ ${passed} tests passed`);
  console.log(`❌ ${failed} tests failed`);
}