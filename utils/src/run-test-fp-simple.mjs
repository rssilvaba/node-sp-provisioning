/* eslint-disable no-sequences */
// @ts-check

import glob from 'glob';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { map } from 'ramda';

const Task = (fork) => ({
  fork,
  ap: (other) => Task((rej, res) => fork(rej, (f) => other.fork(rej, (x) => res(f(x))))),
  map: (f) => Task((rej, res) => fork(rej, (x) => res(f(x)))),
  chain: (f) => Task((rej, res) => fork(rej, (x) => f(x).fork(rej, res))),
  concat: (other) => Task((rej, res) => fork(rej, (x) => other.fork(rej, (y) => res(x.concat(y))))),
  fold: (f, g) => Task((rej, res) => fork((x) => f(x).fork(rej, res), (x) => g(x).fork(rej, res))),
});
Task.fold = (f, g) => (task) => task.fold(f, g);
Task.fork = Task.fold;
Task.chain = (f) => (task) => task.chain(f);
Task.of = (x) => Task((rej, res) => res(x));
Task.rejected = (x) => Task((rej, res) => rej(x));
Task.fromPromised = (fn) => (...args) => Task((rej, res) => fn(...args).then(res).catch(rej));

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

const importModule = (string) => import(`./${path.relative(__dirname, string)}`);
// export const runTest = async (string) => pipe(
//   await Promise.all(glob.sync(string).map(importModule)),
//   A.map((module) => Object.values(module)),
//   A.flatten,
//   A.map(fn => [fn(), fn.name]),
//   a => console.log(a[0])
// )
export const runTest = (globString) => pipe(
  glob.sync(globString),
  map(Task.fromPromised(importModule)),

  // AR.map((module) => Object.values(module)),
  // and then apply the merge
  // AR.map(files=> AR.traverse(id)(files,id))
  // AR.traverse(getApplicative(TE))((module) => Object.values(module)),
  // AR.flatten,
  // AR.map(fn => [fn(), fn.name]),
  async (a) => console.log(await a[0]()),
);

// export async function runTest(globString) {
//   let passed = 0;
//   let failed = 0;
//   const files = glob.sync(globString);
//   for (let i = 0; i < files.length; i++) {
//     // eslint-disable-next-line no-await-in-loop
//     const module = await import(`./${path.relative(__dirname, files[i])}`);
//     for (let z = 0; z < Object.keys(module).length; z++) {
//       const testFn = Object.keys(module)[z];
//       // eslint-disable-next-line no-await-in-loop
//       const result = await module[testFn]()
//       // eslint-disable-next-line no-plusplus
//       if (result) { passed++ } else { failed++ };
//       console.log(`${result ? `✅` : `❌`} ${module[testFn].description || testFn}`)
//     }
//   }
//   console.log(`✅ ${passed} tests passed`);
//   console.log(`❌ ${failed} tests failed`);
// }

runTest('utils/src/pascal-case.test.mjs');
