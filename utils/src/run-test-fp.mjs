/* eslint-disable no-sequences */
// @ts-check
import * as E from 'fp-ts/lib/Either.js';
import * as A from 'fp-ts/lib/ReadonlyArray.js';
import * as TE from 'fp-ts/lib/TaskEither.js';
import * as T from 'fp-ts/lib/Task.js';
import glob from 'glob'
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { flow, pipe, identity as id } from 'fp-ts/lib/function.js';

const spy = x => (console.log(x), x)

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

// export const runTest = async (string) => pipe(
//   await Promise.all(glob.sync(string).map(importModule)),
//   A.map((module) => Object.values(module)),
//   A.flatten,
//   A.map(fn => [fn(), fn.name]),
//   a => console.log(a[0])
// )

// const safeRunFn = (f) => E.tryCatch(() => [f.description || f.name, f()], (e) => [f.description || f.name, e]);
const safeRunFn = f => TE.tryCatchK(
  async (s) => await f() && `✅ ${f.description || f.name} - passed`,
  (e) => `❌ ${f.description || f.name} - failed \n${e}`
)(f);

const safeImport = z => TE.tryCatchK(
  (s) => import(`./${path.relative(__dirname, s)}`),
  (e) => `❌ ${z} - failed \n${e}`
)(z);

export const runTest = (globString) => pipe(
  glob.sync(globString),
  x => [...x, 'tras'],
  A.map(safeImport),
  A.map(TE.map(Object.values)),
  A.map(TE.chain(TE.traverseArray(safeRunFn))),
  A.map(TE.match(id, id)),
  T.sequenceArray,
  T.map(A.flatten)
)

// async x => {
//   const z = await (await x()).flat();
//   z.map(async y => console.log(await y))
// },
const z = runTest('utils/src/**/*.test.mjs');
console.log((await z()).map(async y => console.log(await y)))