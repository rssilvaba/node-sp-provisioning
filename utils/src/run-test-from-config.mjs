// @ts-check
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { browserRemoteDebuggerUrl, pageSelector, pageUrlToMatch } from '../env.mjs';
import { browserPage } from './browser-page.mjs';
import { map, mapAsync } from './fp-utils.mjs';
import { importer } from './importer.mjs';

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

// eslint-disable-next-line no-underscore-dangle
function import_(filePath) {
  return import(`./${path.relative(__dirname, filePath)}`);
}
function runnerUnit(module) {
  const tests = Object.entries(module).filter(([key, test]) => key === 'exampleTests');
  return mapAsync(tests, async ([, f]) => { const r = await f.fn(); return `${r === true ? '✅' : '❌'} ${f.description || f.name}${r !== true ? `\n${r}` : ''}`; });
}
runnerUnit.done = () => { };
export async function runnerInt(module) {
  const tests = Object.values(module).filter((x) => x.name === 'exampleTests');
  return mapAsync(tests, async (f) => {
    const dep = await importer(f.module);
    const r = !(runnerInt.page instanceof Error) && await runnerInt.page.evaluate(f, dep); return `${r === true ? '✅' : '❌'} ${f.description || f.name}${r !== true ? `\n${r}` : ''}`;
  });
}

/** @type {import('puppeteer-core').Page | Error} */
runnerInt.page = await browserPage(browserRemoteDebuggerUrl, pageUrlToMatch, pageSelector);

runnerInt.done = () => {
  // eslint-disable-next-line no-unused-expressions
  !(runnerInt.page instanceof Error) && runnerInt.page.browser().disconnect();
};

export async function runTests(files, runner = runnerUnit) {
  const modulesPromise = map(files, import_);
  const modules = Object.values(await Promise.all(modulesPromise));
  const results = (await Promise.all(map(modules, runner))).flat();
  const failed = results.filter((x) => x.indexOf('❌') > -1).length;
  const passed = results.length - failed;
  const passedLog = passed ? `✅ ${passed} tests passed` : '';
  const failedLog = failed ? `❌ ${failed} tests failed` : '';
  const total = `\n⬜ ${results.length} tests ran`;
  runner?.done?.();
  return [...results, total, passedLog, failedLog].join('\n');
}

runTests(['utils/src/pascal-case.test.mjs']);
