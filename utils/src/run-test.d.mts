export function runnerInt(module: Promise<unknown>): Promise<string[]>;

export namespace runnerInt {
    const page: import('puppeteer-core').Page | Error;
    function done(): void;
}
export function runTests(files: string[], runner?: typeof runnerUnit): Promise<string>;

declare function runnerUnit(module: Promise<unknown>): Promise<string[]>;

declare namespace runnerUnit {
    function done(): void;
}
export {};
