// @ts-check
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { browserRemoteDebuggerUrl, pageSelector, pageUrlToMatch } from '../env.mjs';
import { browserPage } from './browser-page.mjs';
import { map, mapAsync } from './fp-utils.mjs';
import { importer } from './importer.mjs';

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

// eslint-disable-next-line no-underscore-dangle
function import_(filePath) {
  return import(`./${path.relative(__dirname, filePath)}`);
}
function runnerUnit(module) {
  return mapAsync(Object.values(module), async (f) => { const r = await f(); return `${r === true ? '✅' : '❌'} ${f.description || f.name}${r !== true ? `\n${r}` : ''}`; });
}
runnerUnit.done = () => { };
export async function runnerInt(module) {
  return mapAsync(Object.values(module), async (f) => {
    const dep = await importer(f.module);
    const r = !(runnerInt.page instanceof Error) && await runnerInt.page.evaluate(f, dep); return `${r === true ? '✅' : '❌'} ${f.description || f.name}${r !== true ? `\n${r}` : ''}`;
  });
}

runnerInt.page = await browserPage(browserRemoteDebuggerUrl, pageUrlToMatch, pageSelector);

runnerInt.done = () => {
  // eslint-disable-next-line no-unused-expressions
  !(runnerInt.page instanceof Error) && runnerInt.page.browser().disconnect();
};

const padStart = (length) => (num) => String(num).padStart(length, '0');

export async function runTests(files, runner = runnerUnit) {
  const modulesPromise = map(files, import_);
  const modules = Object.values(await Promise.all(modulesPromise));
  const results = (await Promise.all(map(modules, runner))).flat();
  const failed = results.filter((x) => x.indexOf('❌') > -1).length;
  const passed = results.length - failed;
  const total = results.length;
  const pad = padStart(
    Math.max(String(failed).length, String(passed).length, String(total).length),
  );
  const passedLog = passed ? `✅ ${pad(passed)} tests passed` : '';
  const failedLog = failed ? `❌ ${pad(failed)} tests failed` : '';
  const totalLog = `\n⬜ ${pad(results.length)} tests ran`;
  runner?.done?.();
  return [...results, totalLog, passedLog, failedLog].join('\n');
}
