export const fpTsUnpkg = {
  pipe: `https://unpkg.com/fp-ts@2.12.1/es6/function.js`,
  curry: `https://unpkg.com/ramda@0.28.0/es/curry.js`,
  IO: `https://unpkg.com/fp-ts@2.12.1/es6/IO.js`,
}
export const ramdaUnpkg = {
  __: `https://unpkg.com/ramda@0.28.0/es/__.js`
}