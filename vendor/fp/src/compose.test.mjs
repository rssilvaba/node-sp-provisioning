import { compose } from "./compose.mjs"

export const TestCompose01 = () => {
  const add = x => y => z => x + y + z
  const mult = x => y => x * y
  const result = compose(mult(2), add(1)(2))(3)
  return result === 12
}

export const TestCompose02 = () => {
  const add = x => y => z => x + y + z
  const mult = x => y => x * y
  try {
    return compose(mult(2)(3), add(1)(2),)()
  } catch (error) {
    return error.message === `non function: "${6}" passed as argument on position ${0}`
  }
}