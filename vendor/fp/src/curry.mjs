/* eslint-disable no-param-reassign */
/* eslint-disable no-else-return */
/* eslint-disable prefer-const */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-extend-native */

const hasOwnProperty = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

const fToString = (name = '') => (f) => name || f.toString();

Function.prototype.log = function log() {
  const isCurried = hasOwnProperty(this, '_curried');
  const name = hasOwnProperty(this, '_name') ? this._name : this.name;
  let count = [];
  for (let index = 0; index < this?._length; index += 1) {
    count.push(this?._args?.length >= index + 1 ? this?._args[index] : '*')
  }
  if (isCurried) {
    return `${fToString(name)(this)}(${count.join(', ')})`;
  } else {
    return `${fToString(name)(this)}(${count.length > 0 ? count.join(', ') : 'none'})`;
  }
}

export function curry(func, name = func.name) {
  // eslint-disable-next-line no-console
  console.assert(typeof func === 'function');
  func._name = name;
  // func._argNames = func.toString().split('(')[1].split(')')[0].split(',').map(x => x.trim())
  func._length = func.length;
  function curried(...args) {
    function fn2(...args2) {
      return curried.apply(this, args.concat(args2));
    }
    fn2._args = args
    fn2._name = name
    // fn2._argNames = func._argNames
    fn2._curried = true
    fn2._length = func.length
    if (args.length >= func.length) {
      return func.apply(this, args);
    } else {
      return fn2
    }
  }
  curried._name = name
  curried._length = func.length
  return curried;
}
