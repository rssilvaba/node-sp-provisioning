import { curry } from "./curry.mjs";

export const TestCurry01 = () => {
  const add = (x, y, z) => x + y + z;
  const addCurried = curry(add, 'add');
  const add1 = addCurried(1);
  const addExtra = add1(2);
  const result = addExtra(3)
  return addCurried.log() === 'add(*, *, *)'
    && add1.log() === 'add(1, *, *)'
    && addExtra.log() === 'add(1, 2, *)'
    && result === 6
}

export const TestCurry02 = () => {
  const addProp = (obj, prop, value) => Object.assign(obj, { [prop]: value });
  const addPropCurried = curry(addProp, 'addProp');
  const addPropObj = addPropCurried({});
  const addPropObjProp = addPropObj('zed');
  const result = addPropObjProp(5)
  return addPropCurried.log() === 'addProp(*, *, *)'
    && addPropObj.log() === 'addProp([object Object], *, *)'
    && addPropObjProp.log() === 'addProp([object Object], zed, *)'
    && JSON.stringify(result) === JSON.stringify({ zed: 5 })
}


export const TestCurry03 = () => {
  const nothing = () => '';
  const nothingCurried = curry(nothing, 'nothing');
  const result = nothing()
  return nothingCurried.log() === 'nothing(none)'
    && result === ''
}

export const TestCurry04 = () => {
  const nothing = () => '';
  const nothingCurried = curry(nothing, 'nothing');
  const result = nothing()
  return nothingCurried.log() === 'nothing(none)'
    && result === ''
}
export const TestCurry05 = () => {
  const add = (x, y, z) => x + y + z;
  const addCurried = curry(add, 'add')(1, 2);
  const result = addCurried(3)
  return addCurried.log() === 'add(1, 2, *)'
    && result === 6
}