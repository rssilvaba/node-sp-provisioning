// compose :: ((y -> z), (x -> y),  ..., (a -> b)) -> a -> z
export const pipe = (...fns) => (...args) => {
  const noFn = fns.filter(x => typeof x !== 'function');
  const noFnIndex = fns.findIndex(x => typeof x !== 'function');
  if (noFn.length > 0) {
    throw Error(`non function: "${noFn[0]?.log?.() ?? noFn[0].toString()}" passed as argument on position ${noFnIndex}`)
  }
  return fns
    .reduce((res, fn) => [fn.call(null, ...res)], args)[0];
}
