import { pipe } from "./pipe.mjs"

export const TestPipe01 = () => {
  const add = x => y => z => x + y + z
  const mult = x => y => x * y
  const result = pipe(add(1)(2), mult(2),)(3)
  return result === 12
}

export const TestPipe02 = () => {
  const add = x => y => z => x + y + z
  const mult = x => y => x * y
  try {
    return pipe(add(1)(2), mult(2)(3),)()
  } catch (error) {
    return error.message === `non function: "${6}" passed as argument on position ${1}`
  }
}