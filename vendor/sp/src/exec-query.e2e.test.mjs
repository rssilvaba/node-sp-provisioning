// @ts-check

export const execQuerySuccess = async (/** @type {string} */ module_) => {
  /** @type {import('./exec-query.mjs')} */
  const { execQuery } = await import(module_);
  const spContext = new SP.ClientContext();
  const field = spContext.get_web().get_fields().getByInternalNameOrTitle('MobilePhone');
  spContext.load(field);
  const fieldLoaded = await execQuery(spContext, (a, s) => s, () => field);
  return fieldLoaded instanceof SP.Field && fieldLoaded.get_title() === 'Mobile Number';
};
execQuerySuccess.description = 'execQuery - success path';
execQuerySuccess.module = 'vendor/sp/src/exec-query.mjs';

export const execQueryFail = async (/** @type {string} */ module_) => {
  /** @type {import('./exec-query.mjs')} */
  const { execQuery } = await import(module_);
  const spContext = new SP.ClientContext();
  const field = spContext.get_web().get_fields().getByInternalNameOrTitle('MobilePdsdhone');
  spContext.load(field);
  const fieldLoaded = await execQuery(spContext, (a, s) => s, () => field).catch((e) => e);
  return fieldLoaded instanceof SP.ClientRequestFailedEventArgs
    && typeof fieldLoaded.get_message === 'function';
};
execQueryFail.description = 'execQuery - fail path';
execQueryFail.module = 'vendor/sp/src/exec-query.mjs';
