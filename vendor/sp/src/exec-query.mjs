// @ts-check

/**
 * @template A
 * @template E
 * @param {SP.ClientRuntimeContext} ctx
 * @param {(a:SP.ClientRequest,e:SP.ClientRequestFailedEventArgs)=>E} rej
 * @param {(a:SP.ClientRequest,b:SP.ClientRequestSucceededEventArgs)=>A} res
 * @returns {Promise.<A|SP.ClientRequestFailedEventArgs|E>}
 */
// eslint-disable-next-line no-promise-executor-return
export const execQuery = (ctx, rej, res) => new Promise((rs, rj) => ctx
  .executeQueryAsync((...args) => rs(res(...args)), (...args) => rj(rej(...args))));
