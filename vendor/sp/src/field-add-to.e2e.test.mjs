// @ts-check

export const fieldAddToSuccess = async (/** @type {string} */ deps) => {
  /** @type {import('./field-add-to.mjs')} */
  const { fieldAddTo } = await import(deps);
  const spContext = new SP.ClientContext();
  return fieldAddTo(spContext.get_web().get_fields(), '<Field Name="qwe"></Field>') instanceof SP.Field;
};
fieldAddToSuccess.description = 'fieldAddTo - success path';
fieldAddToSuccess.module = 'vendor/sp/src/field-add-to.mjs';
