// @ts-check

/**
 *
 * @param {SP.FieldCollection} to
 * @param {String} xml
 * @param {boolean} [addToDefaultView]
 * @param {SP.AddFieldOptions} [options]
 * @returns {SP.Field}
 */
export const fieldAddTo = (to, xml, addToDefaultView, options) => to
  .addFieldAsXml(xml, addToDefaultView || false, options || 0);
