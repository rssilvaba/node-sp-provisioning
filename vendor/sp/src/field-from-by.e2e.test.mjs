// @ts-check

export const fieldFromBySuccess = async (/** @type {string} */deps) => {
  /** @type {import('./field-from-by.mjs')} */
  const { fieldFromBy } = await import(deps);
  const spContext = new SP.ClientContext();
  const fieldsCollection = spContext.get_web().get_fields();
  const fieldFromWebByInternalName = fieldFromBy(fieldsCollection, 'getByInternalNameOrTitle');
  return fieldFromWebByInternalName('MobilePhone') instanceof SP.Field;
};
fieldFromBySuccess.description = 'fieldFromBy - success path';
fieldFromBySuccess.module = 'vendor/sp/src/field-from-by.mjs';
