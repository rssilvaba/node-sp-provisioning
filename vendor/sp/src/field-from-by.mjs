// @ts-check

/** @typedef {SP.FieldCollection['getByInternalNameOrTitle']} getByInternalNameT */
/** @typedef {SP.FieldCollection['getById']} getByIdT */
/** @typedef {SP.FieldCollection['getByTitle']} getByTitleT */

/**
 * @param {SP.FieldCollection} from
 * @param {('getByTitle' | 'getById' | 'getByInternalNameOrTitle')} by
 * @returns {getByInternalNameT & getByTitleT & getByIdT}
 */
// @ts-ignore, function overload working weird here
export const fieldFromBy = (from, by) => from[by].bind(from);
