// @ts-check

export async function fieldSetPropSuccess(deps) {
  /** @type {import('./field-set-prop.mjs')} */
  const { fieldSetProp } = await import(deps);
  const spContext = new SP.ClientContext();
  /** @type {any} */
  const field = spContext.get_web().get_fields().getByInternalNameOrTitle('MobilePhone');
  spContext.load(field);
  fieldSetProp('title', '123456', field);
  return field.get_objectData().get_properties().title === '123456';
}
fieldSetPropSuccess.description = 'fieldSetProp - success path';
fieldSetPropSuccess.module = 'vendor/sp/src/field-set-prop.mjs';
