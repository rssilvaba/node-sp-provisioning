// @ts-check

/** @typedef {{ get_objectData:()=>SP.ClientObjectData }} objectData */


/**
 * @template A
 * @param {string} prop 
 * @param {A} value 
 * @param {SP.Field & objectData } field 
 */
export const fieldSetProp = (prop, value, field) => {
  const fieldProps = field.get_objectData().get_properties();
  fieldProps[prop] = value;
  field.get_context().addQuery(new SP.ClientActionSetProperty(field, prop, value));
} 