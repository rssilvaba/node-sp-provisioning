// @ts-check

// export const module = ;

export async function loadSuccess(deps) {
  /** @type {import('./load.mjs')} */
  const { load } = await import(deps);
  const spContext = new SP.ClientContext();
  const field = spContext.get_web().get_fields().getByInternalNameOrTitle('MobilePhone');
  load(spContext, field);
  /** @type {SP.Field} */
  const fieldLoaded = await new Promise(
    // eslint-disable-next-line no-promise-executor-return
    (rs, rj) => spContext.executeQueryAsync(() => rs(field), (a) => rj(a))
  );
  return fieldLoaded instanceof SP.Field
}
loadSuccess.description = 'load - success path';
loadSuccess.module = 'vendor/sp/src/load.mjs';


// export async function E2eTestLoadSuccessCurried(/** @type {string} */ module_) {
//   const fpTsUnpkg = {
//     pipe: `https://unpkg.com/fp-ts@2.12.1/es6/function.js`,
//     curry: `https://unpkg.com/ramda@0.28.0/es/curry.js`,
//     IO: `https://unpkg.com/fp-ts@2.12.1/es6/IO.js`,
//   }
//   const ramdaUnpkg = {
//     __: `https://unpkg.com/ramda@0.28.0/es/__.js`
//   }
//   /** @type {import('./load.mjs')} */
//   const { load } = await import(module_);
//   /** @type {import('ramda').curry} */
//   const curry = (await import(fpTsUnpkg.curry)).default;
//   /** @type {import('ramda').__} */
//   const __ = (await import(ramdaUnpkg.__)).default;
//   const spContext = new SP.ClientContext();
//   const loadCurried = curry(load);
//   const field = spContext.get_web().get_fields().getByInternalNameOrTitle('MobilePhone');
//   const fieldLoaded = await new Promise(
//     (rs, rj) => spContext.executeQueryAsync(() => rs(field), (a) => rj(a))
//   );
//   loadCurried(spContext)(field,);
//   return fieldLoaded instanceof SP.Field
// }
