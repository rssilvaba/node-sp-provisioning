/**
 * 
 * @param {SP.ClientRuntimeContext} ctx 
 * @param {SP.ClientObject} rsc 
 * @param {string[]=} args 
 * @returns {void}
 */
export const load = (ctx, rsc, args) => args ? ctx.load(rsc, args) : ctx.load(rsc);